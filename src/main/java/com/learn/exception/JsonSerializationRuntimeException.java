package com.learn.exception;

/**
 * @className JsonSerializationRuntimeExcetion
 * @description json序列化异常
 */
public class JsonSerializationRuntimeException extends RuntimeException {
  public JsonSerializationRuntimeException() {
    super();
  }

  public JsonSerializationRuntimeException(String message) {
    super(message);
  }

  public JsonSerializationRuntimeException(String message, Throwable cause) {
    super(message, cause);
  }

  public JsonSerializationRuntimeException(Throwable cause) {
    super(cause);
  }

}

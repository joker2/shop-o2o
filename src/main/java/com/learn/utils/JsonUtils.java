package com.learn.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.exception.JsonSerializationRuntimeException;
import com.learn.pojo.Shop;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * json处理工具类
 */
public class JsonUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);
  private static ObjectMapper mapper;

  static {
    mapper = new ObjectMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
  }

  private JsonUtils() {
  }

  public static String objectToJson(Object object) {
    try {
      return mapper.writeValueAsString(object);
    } catch (JsonProcessingException e) {
      LOGGER.error("json 转换异常,Object:{[]}", object);
      throw new JsonSerializationRuntimeException(e.getMessage());
    }
  }

  public static <T> T readValue(String jsonStr, Class<T> valueType) {
    try {
      return mapper.readValue(jsonStr, valueType);
    } catch (IOException e) {
      LOGGER.error("json 转换异常,json字符串:{[]}，类型:{[]}", jsonStr, valueType);
      throw new JsonSerializationRuntimeException(e.getMessage());
    }
  }

  public static <T> T readValue(String jsonStr, TypeReference<T> valueTypeRef) {
    try {
      return mapper.readValue(jsonStr, valueTypeRef);
    } catch (IOException e) {
      LOGGER.error("json 转换异常,json字符串:{[]}，类型:{[]}", jsonStr, valueTypeRef);
      throw new JsonSerializationRuntimeException(e.getMessage());
    }
  }

  public static <T> T convertValue(Object obj, Class<T> valueType) {
    try {
      return mapper.convertValue(obj, valueType);
    } catch (IllegalArgumentException e) {
      LOGGER.error("json 转换异常,Object:{[]}，类型:{[]}", obj, valueType);
      throw new JsonSerializationRuntimeException(e.getMessage());
    }
  }
}

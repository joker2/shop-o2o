package com.learn.utils;

import javax.servlet.http.HttpServletRequest;

/**
* @author wudhk
* @description 验证码验证
* @date 2018/12/12
*/
public class VerifyCodeUtil {

  public static boolean checkVerifyCode(HttpServletRequest request) {
    String verifyCodeExpected = (String) request.getSession().getAttribute(
        com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
    String verifyCodeActual = HttpServletRequestUtil.getString(request, "verifyCodeActual");
    if (verifyCodeActual == null || !verifyCodeActual.equalsIgnoreCase(verifyCodeExpected)) {
      return false;
    }
    return true;
  }
}

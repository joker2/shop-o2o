package com.learn.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
public class FileUtil {

  private static Logger logger = LoggerFactory.getLogger(FileUtil.class);

  private static String seperator = System.getProperty("file.separator");
  // 时间格式化的格式
  private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

  /**
   * @param type
   * @param id
   * @return java.lang.String
   * @description 获取路径
   */
  public static String getBasePath(String type, String id) {
    StringBuilder pathBuilder = new StringBuilder();
    pathBuilder.append("/home/upload/images/item/");
    pathBuilder.append(type);
    if(id!=null){
      pathBuilder.append("/");
      pathBuilder.append(id);
      pathBuilder.append("/");
    }
    pathBuilder.toString().replace("/", seperator);
    logger.info("basePath is:"+pathBuilder);
    return pathBuilder.toString();
  }

  /**
   * @return java.lang.String
   * @description 生成随机文件名：当前年月日时分秒+五位随机数
   */
  public static String getRandomFileName() {
    Random random = new Random();
    // 获取随机数
    int rannum = random.nextInt(89999) + 10000;
    // 当前时间
    String nowTimeStr = sDateFormat.format(new Date());
    return nowTimeStr + rannum;
  }

  /**
   * @return void
   * @description 创建文件路径
   */
  public static void makeDirPath(String targetAddr) {
    File dirPath = new File(targetAddr);
    if (!dirPath.exists()) {
      dirPath.mkdirs();
    }
  }

  /**
   * @param request
   * @return org.springframework.web.multipart.commons.CommonsMultipartFile
   * @description 获取文件
   */
  public static CommonsMultipartFile getMultipartFile(HttpServletRequest request) {
    MultipartHttpServletRequest multipartRequest = null;
    CommonsMultipartFile multipartFile = null;
    CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(
        request.getSession().getServletContext());
    if (multipartResolver.isMultipart(request)) {
      multipartRequest = (MultipartHttpServletRequest) request;
      multipartFile = (CommonsMultipartFile) multipartRequest.getFile("file");
    }
    return multipartFile;
  }

  /**
   * @param storePath
   * @return void
   * @description 文件删除
   */
  public static void deleteFile(String storePath) {
    File file = new File(storePath);
    if (file.exists()) {
      if (file.isDirectory()) {
        File files[] = file.listFiles();
        for (int i = 0; i < files.length; i++) {
          files[i].delete();
        }
      }
      file.delete();
    }
  }
}

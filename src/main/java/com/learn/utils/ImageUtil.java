package com.learn.utils;

import java.io.File;
import java.io.IOException;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
public class ImageUtil {

  private static Logger logger = LoggerFactory.getLogger(ImageUtil.class);

  public static String generateThumbnail(CommonsMultipartFile multipartFile, String targetAddr) {
    // 获取文件扩展名
    String extname = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
    // 生成文件名
    String fileName = FileUtil.getRandomFileName();
    logger.info("current fileName is :" + fileName+"."+extname);
    String allowImgFormat = "gif,jpg,jpeg,png";
    if (!allowImgFormat.contains(extname.toLowerCase()) || "".equals(extname.trim())) {
      throw new RuntimeException("图片格式不规范！");
    }
    // 创建路径
    FileUtil.makeDirPath(targetAddr);
    // 文件路径
    String relativePath = targetAddr + fileName + extname;
    logger.info("current relativePath is :" + relativePath);
    File dest = new File(relativePath);
    try {
      Thumbnails.of(multipartFile.getInputStream()).size(200, 200).outputQuality(0.25f)
          .toFile(dest);
    } catch (IOException e) {
      throw new RuntimeException("创建缩略图失败：" + e.toString());
    }
    return relativePath;
  }
}

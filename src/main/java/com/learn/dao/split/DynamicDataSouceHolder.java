package com.learn.dao.split;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/12
 */
public class DynamicDataSouceHolder {

  private static Logger logger = LoggerFactory.getLogger(DynamicDataSouceHolder.class);
  private static ThreadLocal<String> contextHolder = new ThreadLocal<>();
  public static final String DB_MASTER = "master";
  public static final String DB_SLAVE = "slave";

  /**
   * @return java.lang.String
   * @description 获取连接类型
   */
  public static String getDbType() {
    String db = contextHolder.get();
    if (StringUtils.isEmpty(db)) {
      db = DB_MASTER;
    }
    return db;
  }

  /**
   * @return void
   * @description 设置连接类型
   */
  public static void setDbType(String dbType) {
    logger.debug("所使用的数据源为：" + dbType);
    contextHolder.set(dbType);
  }

  /**
   * @return void
   * @description 清理连接类型
   */
  public static void clearDbType() {
    contextHolder.remove();
  }
}

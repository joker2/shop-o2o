package com.learn.dao;

import com.learn.pojo.Area;
import java.util.List;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/4
 */
public interface AreaMapper {

  List<Area> queryArea();
}

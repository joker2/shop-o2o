package com.learn.dao;

import com.learn.pojo.ProductCategory;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author wudhk
 * @descriptin 商品类别 Mapper
 * @date 2018/12/14
 */
public interface ProductCategoryMapper {

  /**
   * @return java.util.List<com.learn.pojo.ProductCategory>
   * @description 通过shopId查询商品类别
   */
  List<ProductCategory> queryProductCategoryByShopId(long shopId);

  int batchInsertProductCategory(List<ProductCategory> productCategoryList);

  int deleteProductCategory(@Param("productCategoryId") long productCategoryId,
      @Param("shopId") long shopId);
}

package com.learn.dao;

import com.learn.pojo.ShopCategory;
import java.util.List;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/6
 */
public interface ShopCategoryMapper {

  List<ShopCategory> queryShopCategory(ShopCategory shopCategory);
}

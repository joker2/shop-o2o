package com.learn.dao;

import com.learn.pojo.Shop;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
public interface ShopMapper {

  /**
   * @param shopCondition
   * @return java.util.List<com.learn.pojo.Shop>
   * @description 查询店铺列表
   */
  List<Shop> queryShopList(@Param("shopCondition") Shop shopCondition);

  /**
   * @param id
   * @return com.learn.pojo.Shop
   * @description 根据id查询店铺信息
   */
  Shop queryByShopId(long id);

  /**
   * @param shop
   * @return int
   * @description 新增店铺
   */
  int insertShop(Shop shop);

  /**
   * @param shop
   * @return int
   * @description 更新店铺
   */
  int updateShop(Shop shop);
}

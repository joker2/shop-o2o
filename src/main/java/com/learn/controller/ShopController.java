package com.learn.controller;

import com.learn.common.Result;
import com.learn.enums.HttpStatusEnum;
import com.learn.exception.JsonSerializationRuntimeException;
import com.learn.pojo.PersonInfo;
import com.learn.pojo.Shop;
import com.learn.service.ShopService;
import com.learn.utils.FileUtil;
import com.learn.utils.HttpServletRequestUtil;
import com.learn.utils.JsonUtils;
import com.learn.utils.VerifyCodeUtil;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
@Controller
@RequestMapping("/shop")
public class ShopController {

  @Autowired
  private ShopService shopService;

  @ResponseBody
  @PostMapping("/registershop")
  private String registerShop(HttpServletRequest request) {
    try {
      boolean verifyCode = VerifyCodeUtil.checkVerifyCode(request);
      if (!verifyCode) {
        return Result.setResult(HttpStatusEnum.ERROR, "验证码错误");
      }
      String shopJson = request.getParameter("shopJson");
      Shop shop = JsonUtils.readValue(shopJson, Shop.class);

      CommonsMultipartFile multipartFile = FileUtil.getMultipartFile(request);
      PersonInfo user = (PersonInfo) request.getSession().getAttribute("user");
      shop.setPersonInfo(user);
      return shopService.addShop(shop, multipartFile);
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }

  @ResponseBody
  @GetMapping("/getshopbyid")
  private String getShopById(HttpServletRequest request) {
    try {
      Long shopId = HttpServletRequestUtil.getLong(request, "shopId");
      if (shopId > -1) {
        Shop shop = shopService.getByShopId(shopId);
        return Result.setResult(HttpStatusEnum.OK, shop);
      }
      return Result.setResult(HttpStatusEnum.ERROR, null);
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }

  @ResponseBody
  @PostMapping("/modifyshop")
  private String modifyShop(HttpServletRequest request) {
    try {
      boolean verifyCode = VerifyCodeUtil.checkVerifyCode(request);
      if (!verifyCode) {
        return Result.setResult(HttpStatusEnum.ERROR, "验证码错误");
      }
      String shopJson = request.getParameter("shopJson");
      Shop shop = JsonUtils.readValue(shopJson, Shop.class);

      CommonsMultipartFile multipartFile = FileUtil.getMultipartFile(request);

      return shopService.modifyShop(shop, multipartFile);
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }

  @ResponseBody
  @GetMapping("/getshoplist")
  private String getShopList(HttpServletRequest request) {
    try {
      PersonInfo user = (PersonInfo) request.getSession().getAttribute("user");
      //int pageNum = HttpServletRequestUtil.getInt(request, "pageNum");
      //int pageSize = HttpServletRequestUtil.getInt(request, "pageSize");
      int pageNum = 1;
      int pageSize = 100;
      Shop shop = new Shop();
      shop.setPersonInfo(user);
      return shopService.getShopList(shop, pageNum, pageSize);
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }

  @ResponseBody
  @GetMapping("/getshopInfo")
  private Map<String, Object> getShopManagementInfo(HttpServletRequest request) {
    Map<String, Object> modelMap = new HashMap<>();
    long shopId = HttpServletRequestUtil.getLong(request, "shopId");
    if (shopId <= 0) {
      Object currentShopObj = request.getSession().getAttribute("currentShop");
      if (currentShopObj == null) {
        modelMap.put("redirect", true);
        modelMap.put("url", "/shop/shoplist");
      } else {
        Shop currentShop = (Shop) currentShopObj;
        modelMap.put("redirect", false);
        modelMap.put("shopId", currentShop.getShopId());
      }
    } else {
      Shop currentShop = new Shop();
      currentShop.setShopId(shopId);
      request.getSession().setAttribute("currentShop", currentShop);
      modelMap.put("redirect", false);
    }
    return modelMap;
  }
}

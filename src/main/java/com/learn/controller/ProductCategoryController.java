package com.learn.controller;

import com.learn.common.Result;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.ProductCategory;
import com.learn.pojo.Shop;
import com.learn.service.ProductCategoryService;
import com.learn.utils.HttpServletRequestUtil;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductCategoryController {

  @Autowired
  private ProductCategoryService productCategoryService;

  @GetMapping("/listproductcategorys")
  private String listProductCategorys(HttpServletRequest request) {
    try {
      Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
      List<ProductCategory> list = null;
      if (currentShop != null && currentShop.getShopId() > 0) {
        return productCategoryService.getByProductCategoryShopId(currentShop.getShopId());
      }
      return Result.setResult(HttpStatusEnum.ERROR, "获取商品类别信息失败");
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }

  @PostMapping("/addproductcategorys")
  private String addProductCategorys(@RequestBody List<ProductCategory> productCategoryList,
      HttpServletRequest request) {
    try {
      Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
      if (productCategoryList != null && productCategoryList.size() > 0 && currentShop != null
          && currentShop.getShopId() > 0) {
        for (ProductCategory pc : productCategoryList) {
          pc.setShopId(currentShop.getShopId());
          pc.setCreateTime(new Date());
          pc.setLastEditTime(new Date());
        }
        return productCategoryService.batchAddProductCategory(productCategoryList);
      }
      return Result.setResult(HttpStatusEnum.ERROR, "添加的商品类别为空");
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }

  @GetMapping("/removeproductcategory")
  private String removeProductCategory(HttpServletRequest request) {
    try {
      Long productCategoryId = HttpServletRequestUtil.getLong(request, "productCategoryId");
      Shop currentShop = (Shop) request.getSession().getAttribute("currentShop");
      if (currentShop != null && currentShop.getShopId() > 0) {
        return productCategoryService
            .deleteProductCategory(productCategoryId, currentShop.getShopId());
      }
      return Result.setResult(HttpStatusEnum.ERROR, "店铺信息为空");
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }
}

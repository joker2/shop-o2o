package com.learn.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/14
 */
@Controller
@RequestMapping("/shop")
public class ShopPageController {

  @GetMapping("/operation")
  private String shopOperation() {
    return "shop/shopOperation";
  }

  @GetMapping("/shoplist")
  private String shopList() {
    return "shop/shoplist";
  }

  @GetMapping("/shopmanage")
  private String shopManage() {
    return "/shop/shopmanage";
  }

  @GetMapping("/productcategorymanage")
  private String productCategoryManage() {
    return "/shop/productcategorymanage";
  }

}

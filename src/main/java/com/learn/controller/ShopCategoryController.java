package com.learn.controller;

import com.learn.common.Result;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.ShopCategory;
import com.learn.service.ShopCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/6
 */
@Controller
@RequestMapping("/shopCategory")
public class ShopCategoryController {
  @Autowired
  private ShopCategoryService categoryService;

  @GetMapping("/getShopCategory")
  @ResponseBody
  public String getShopCategory() {
    try {
      return categoryService.getShopCategoryList(new ShopCategory());
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }
}

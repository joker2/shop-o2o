package com.learn.controller;

import com.learn.common.Result;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.Area;
import com.learn.service.AreaService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/4
 */
@Controller
@RequestMapping("/area")
public class AreaController {

  @Autowired
  private AreaService areaService;

  @GetMapping("/listAreas")
  @ResponseBody
  private String listAreas() {
    try {
      return areaService.getAreaList();
    } catch (Exception e) {
      return Result.setResult(HttpStatusEnum.ERROR, e.getMessage());
    }
  }
}

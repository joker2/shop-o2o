package com.learn.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * @author wudhk
 * @description 商品类别
 * @date 2018/12/4
 */
@Data
public class ProductCategory implements Serializable {

  private static final long serialVersionUID = -2274005539629006323L;

  private Long productCategoryId;
  private Long shopId;
  private String productCategoryName;
  private String productCategoryDesc;
  private Integer priority;
  private Date createTime;
  private Date lastEditTime;

  @Override
  public String toString() {
    return "[productCategoryId=" + productCategoryId
        + ", productCategoryIdName=" + productCategoryName + "]";
  }

}

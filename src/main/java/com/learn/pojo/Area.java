package com.learn.pojo;

import java.util.Date;
import lombok.Data;

/**
* @author wudhk
* @description 区域
* @date 2018/12/4
*/
@Data
public class Area {
	private Long areaId;
	private String areaName;
	private String areaDesc;
	private Integer priority;
	private Date createTime;
	private Date lastEditTime;

}

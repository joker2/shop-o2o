package com.learn.pojo;

import java.util.Date;
import lombok.Data;

/**
 * @author wudhk
 * @description 店铺类别
 * @date 2018/12/4
 */
@Data
public class ShopCategory {

  private Long shopCategoryId;
  private String shopCategoryName;
  private String shopCategoryDesc;
  private String shopCategoryImg;
  private Integer priority;
  private Date createTime;
  private Date lastEditTime;
  private Long parentId;

}

package com.learn.pojo;

import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author wudhk
 * @description 店铺
 * @date 2018/12/4
 */
@Data
public class Shop {

  /**
   * 店铺ID
   */
  private Long shopId;
  /**
   * 店铺创建人
   */
  private PersonInfo personInfo;
  /**
   * 店铺类别
   */
  @NotEmpty(message = "店铺类别不能为空")
  private ShopCategory shopCategory;
  /**
   * 店铺名称
   */
  @NotEmpty(message = "店铺名称不能为空")
  private String shopName;
  /**
   * 店铺描述
   */
  private String shopDesc;
  /**
   * 店铺地址
   */
  @NotEmpty(message = "店铺地址不能为空")
  private String shopAddr;
  /**
   * 店铺电话
   */
  @NotEmpty(message = "店铺电话不能为空")
  private String phone;
  /**
   * 店铺图片
   */
  private String shopImg;
  /**
   * 经度
   */
  private Double longitude;
  /**
   * 维度
   */
  private Double latitude;
  /**
   * 权重
   */
  private Integer priority;
  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 修改时间
   */
  private Date lastEditTime;
  /**
   * 店铺状态
   */
  private Integer enableStatus;
  /**
   * 店铺审核进度
   */
  private String advice;

  /**
   * 店铺所在区域
   */
  @NotEmpty(message = "店铺所在区域不能为空")
  private Area area;

  private List<ShopAuthMap> staffList;

  private ShopCategory parentCategory;

}

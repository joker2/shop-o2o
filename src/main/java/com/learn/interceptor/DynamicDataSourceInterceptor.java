package com.learn.interceptor;

import com.learn.dao.split.DynamicDataSouceHolder;
import java.util.Locale;
import java.util.Properties;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.keygen.SelectKeyGenerator;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * @author wudhk
 * @descriptin mybatis拦截器
 * @date 2018/12/12
 */
@Intercepts({@Signature(type = Executor.class, method = "update", args = {MappedStatement.class,
    Object.class}),
    @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class,
        RowBounds.class, ResultHandler.class})})
public class DynamicDataSourceInterceptor implements Interceptor {

  private static Logger logger = LoggerFactory.getLogger(DynamicDataSourceInterceptor.class);
  private static final String REGEX = ".*insert\\u0020.*||.*delete\\u0020|.*update\\u0020.*";

  @Override
  public Object intercept(Invocation invocation) throws Throwable {
    // 判断方法是否是被事务管理的
    boolean synchronizationAction = TransactionSynchronizationManager.isActualTransactionActive();
    Object[] objects = invocation.getArgs();
    MappedStatement mappedStatement = (MappedStatement) objects[0];
    String lookupKey = DynamicDataSouceHolder.DB_MASTER;
    if (!synchronizationAction) {
      // 读方法
      if (mappedStatement.getSqlCommandType().equals(SqlCommandType.SELECT)) {
        //selectKey为自增id查询主键（SELECT LAST_INSERT_ID）方法
        if (mappedStatement.getId().contains(SelectKeyGenerator.SELECT_KEY_SUFFIX)) {
          lookupKey = DynamicDataSouceHolder.DB_MASTER;
        } else {
          BoundSql boundSql = mappedStatement.getSqlSource().getBoundSql(objects[1]);
          String sql = boundSql.getSql().toLowerCase(Locale.CHINA).replace("\\t\\n\\r", " ");
          if (sql.matches(REGEX)) {
            lookupKey = DynamicDataSouceHolder.DB_MASTER;
          } else {
            lookupKey = DynamicDataSouceHolder.DB_SLAVE;
          }
        }
      }
    } else {
      lookupKey = DynamicDataSouceHolder.DB_MASTER;
    }
    logger.debug("设置方法[{}] use [{}] Strategy,SqlCommandType [{}]...", mappedStatement.getId(),
        lookupKey, mappedStatement.getSqlCommandType().name());
    DynamicDataSouceHolder.setDbType(lookupKey);
    return invocation.proceed();
  }

  @Override
  public Object plugin(Object target) {
    if (target instanceof Executor) {
      return Plugin.wrap(target, this);
    } else {
      return target;
    }
  }

  @Override
  public void setProperties(Properties properties) {

  }
}

package com.learn.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.learn.enums.HttpStatusEnum;
import com.learn.utils.JsonUtils;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Data;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */

@Data
public class Result implements Serializable {

  private static ObjectMapper objectMapper = new ObjectMapper();
  private int code;

  private String message;

  private String data;

  public static String setResult(HttpStatusEnum httpStatusEnum, Object data) {
    Map<String, Object> result = new LinkedHashMap<String, Object>(16);
    result.put("code", httpStatusEnum.getCode());
    result.put("msg", httpStatusEnum.getMsg());
    result.put("data", data);
    String responseData = JsonUtils.objectToJson(result);
    return responseData;
  }
}

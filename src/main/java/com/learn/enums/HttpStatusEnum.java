package com.learn.enums;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
@Getter
@AllArgsConstructor
public enum HttpStatusEnum {

  /*============================标准规则===============================*/
  OK(0, "成功"),
  ERROR(1, "失败");

  /*============================用户规则===============================*/

  private Integer code;
  private String msg;
}

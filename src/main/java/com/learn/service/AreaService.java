package com.learn.service;

import com.learn.pojo.Area;
import java.util.List;

public interface AreaService {
	
	/**
	 * @param 
	 * @return java.util.List<com.learn.pojo.Area>
	 * @description 
	 */
	String getAreaList();

}

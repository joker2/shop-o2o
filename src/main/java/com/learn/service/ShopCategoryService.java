package com.learn.service;

import com.learn.pojo.ShopCategory;
import java.util.List;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/6
 */
public interface ShopCategoryService {

  String getShopCategoryList(ShopCategory shopCategory);
}

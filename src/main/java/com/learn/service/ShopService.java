package com.learn.service;

import com.learn.pojo.Shop;
import java.io.File;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
public interface ShopService {

  Shop getByShopId(long id);

  String modifyShop(Shop shop,CommonsMultipartFile shopImg);

  String addShop(Shop shop, CommonsMultipartFile shopImg);

  String getShopList(Shop shopCondition, int pageNum, int pageSize);
}

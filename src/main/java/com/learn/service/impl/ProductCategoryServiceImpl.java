package com.learn.service.impl;


import com.learn.common.Result;
import com.learn.dao.ProductCategoryMapper;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.ProductCategory;
import com.learn.service.ProductCategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

  @Autowired
  ProductCategoryMapper productCategoryMapper;

  @Override
  public String getByProductCategoryShopId(long shopId) {
    List<ProductCategory> productCategoryList = productCategoryMapper
        .queryProductCategoryByShopId(shopId);
    return Result.setResult(HttpStatusEnum.OK, productCategoryList);
  }

  @Override
  @Transactional
  public String batchAddProductCategory(List<ProductCategory> productCategoryList) {
    try {
      int effectedNum = productCategoryMapper.batchInsertProductCategory(productCategoryList);
      if (effectedNum <= 0) {
        throw new RuntimeException("创建商品类别失败");
      }
      return Result.setResult(HttpStatusEnum.OK, "创建成功");
    } catch (Exception e) {
      throw new RuntimeException("batchAddProductCategory error: " + e.getMessage());
    }
  }

  @Override
  public String deleteProductCategory(long productCategoryId, long shopId) {
    try {
      int effectedNum = productCategoryMapper.deleteProductCategory(productCategoryId, shopId);
      if (effectedNum < 0) {
        throw new RuntimeException("商品类别删除失败");
      }
      return Result.setResult(HttpStatusEnum.OK,"删除成功");
    } catch (Exception e) {
      throw new RuntimeException("deleteProductCategory error: " + e.getMessage());
    }
  }
}

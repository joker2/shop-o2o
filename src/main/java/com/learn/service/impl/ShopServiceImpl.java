package com.learn.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.common.Result;
import com.learn.dao.ShopMapper;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.PersonInfo;
import com.learn.pojo.Shop;
import com.learn.service.ShopService;
import com.learn.utils.FileUtil;
import com.learn.utils.ImageUtil;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/5
 */
@Service
public class ShopServiceImpl implements ShopService {


  @Autowired
  private ShopMapper shopMapper;

  @Override
  public Shop getByShopId(long id) {
    return shopMapper.queryByShopId(id);
  }

  @Override
  @Transactional
  public String modifyShop(Shop shop, CommonsMultipartFile shopImg) {
    if (shop == null || shop.getShopId() == null) {
      return Result.setResult(HttpStatusEnum.ERROR, "店铺信息不能为空");
    } else {
      try {
        //是否需处理图片
        if (shopImg != null) {
          Shop tempShop = shopMapper.queryByShopId(shop.getShopId());
          if (tempShop.getShopImg() != null) {
            //删除旧的图片
            FileUtil.deleteFile(tempShop.getShopImg());
          }
          addShopImg(shop, shopImg);
        }
        //更新店铺
        shop.setLastEditTime(new Date());
        int flag = shopMapper.updateShop(shop);
        if (flag <= 0) {
          throw new RuntimeException("修改店铺失败");
        }
        return Result.setResult(HttpStatusEnum.OK, shop);
      } catch (Exception e) {
        throw new RuntimeException("modifyShop error: " + e.getMessage());
      }
    }
  }

  @Override
  @Transactional
  public String addShop(Shop shop, CommonsMultipartFile shopImg) {
    try {
      PersonInfo personInfo = new PersonInfo();
      personInfo.setUserId(1L);
      shop.setPersonInfo(personInfo);
      shop.setEnableStatus(0);
      shop.setCreateTime(new Date());
      shop.setLastEditTime(new Date());
      int flag = shopMapper.insertShop(shop);
      if (flag <= 0) {
        throw new RuntimeException("店铺创建失败");
      } else {
        try {
          if (shopImg != null) {
            addShopImg(shop, shopImg);
            flag = shopMapper.updateShop(shop);
            if (flag <= 0) {
              throw new RuntimeException("添加图片失败");
            }
          }
        } catch (Exception e) {
          throw new RuntimeException("addShopImg error: " + e.getMessage());
        }
      }
      return Result.setResult(HttpStatusEnum.OK, shop);
    } catch (Exception e) {
      throw new RuntimeException("insertShop error: " + e.getMessage());
    }
  }

  @Override
  public String getShopList(Shop shopCondition, int pageNum, int pageSize) {
    PageHelper.startPage(pageNum, pageSize);
    List<Shop> shopList = shopMapper.queryShopList(shopCondition);
    PageInfo<Shop> pageInfo = new PageInfo<>(shopList);
    return Result.setResult(HttpStatusEnum.OK, pageInfo);
  }

  private void addShopImg(Shop shop, CommonsMultipartFile shopImg) {
    String dest = FileUtil.getBasePath("shop", shop.getShopId().toString());
    String shopImgAddr = ImageUtil.generateThumbnail(shopImg, dest);
    shop.setShopImg(shopImgAddr);
  }
}

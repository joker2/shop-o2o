package com.learn.service.impl;

import com.learn.common.Result;
import com.learn.dao.AreaMapper;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.Area;
import com.learn.service.AreaService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AreaServiceImpl implements AreaService {

  Logger logger = LoggerFactory.getLogger(AreaServiceImpl.class);

  @Autowired
  private AreaMapper areaMapper;

  @Override
  public String getAreaList() {
    List<Area> areaList = areaMapper.queryArea();
    return Result.setResult(HttpStatusEnum.OK,areaList);
  }

}

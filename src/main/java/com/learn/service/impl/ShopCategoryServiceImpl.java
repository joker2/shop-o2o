package com.learn.service.impl;

import com.learn.common.Result;
import com.learn.dao.ShopCategoryMapper;
import com.learn.enums.HttpStatusEnum;
import com.learn.pojo.ShopCategory;
import com.learn.service.ShopCategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author wudhk
 * @descriptin
 * @date 2018/12/6
 */
@Service
public class ShopCategoryServiceImpl implements ShopCategoryService {

  @Autowired
  private ShopCategoryMapper shopCategoryMapper;

  @Override
  public String getShopCategoryList(ShopCategory shopCategory) {
    List<ShopCategory> shopCategoryList = shopCategoryMapper.queryShopCategory(shopCategory);
    return Result.setResult(HttpStatusEnum.OK,shopCategoryList);
  }
}

package com.learn.service;


import com.learn.pojo.ProductCategory;
import java.util.List;

/**
 * @author wudhk
 * @description
 * @date 2018/12/17
 */
public interface ProductCategoryService {

  /**
   * @return java.util.List<com.learn.pojo.ProductCategory>
   * @description 查询指定某个店铺下的所有商品类别信息
   */
  String getByProductCategoryShopId(long shopId);

  String batchAddProductCategory(List<ProductCategory> productCategoryList);

  String deleteProductCategory(long productCategoryId, long shopId);
}

/**
 *
 */
$(function () {
  var shopId = getQueryString('shopId');
  var isEdit = shopId ? true : false;
  var shopInfoUrl = '/shop/getshopbyid?shopId=' + shopId;
  var listShopCategoryUrl = '/shopCategory/getShopCategory';
  var listAreasUrl = '/area/listAreas';
  var editShopUrl = '/shop/registershop';
  if (isEdit) {
    var editShopUrl = '/shop/modifyshop';
    getInfo(shopId);
  } else {
    getAreaInfo();
    getShopCategoryInfo;
  }

  function getInfo(shopId) {
    $.ajaxSettings.async = false;
    $.getJSON(shopInfoUrl, function (data) {
      if (data.code == 0) {
        var shop = data.data;
        $('#shopName').val(shop.shopName);
        $('#shopAddr').val(shop.shopAddr);
        $('#shopPhone').val(shop.phone);
        $('#shopDesc').val(shop.shopDesc);
        getAreaInfo();
        var shopCategory = '<option data-id="'
            + shop.shopCategory.shopCategoryId + '" selected>'
            + shop.shopCategory.shopCategoryName + '</option>';
        var opts = $('#area')[0];
        for (var i = 0; i < opts.options.length; i++) {
          if (shop.area.areaId == opts.options[i].index) {
            opts.options[i].selected = 'selected';
          }
        }
        $('#shopCategory').html(shopCategory);
        $('#shopCategory').attr('disabled', 'disabled');
      }else{
        $.toast('修改失败：'+data.data);
      }
    })
  }

  function getAreaInfo() {
    $.getJSON(listAreasUrl, function (data) {
      if (data.code == 0) {
        var tempAreaHtml = '<option></option>';
        data.data.map(function (item, index) {
          tempAreaHtml += '<option data-id="' + item.areaId + '">'
              + item.areaName + '</option>';
        });
        $('#area').html(tempAreaHtml);
      } else {
        $.toast('获取失败！');
      }
    });
  }

  function getShopCategoryInfo() {
    $.getJSON(listShopCategoryUrl, function (data) {
      if (data.code == 0) {
        var tempHtml = '<option></option>';
        data.data.map(function (item, index) {
          tempHtml += '<option data-id="' + item.shopCategoryId + '">'
              + item.shopCategoryName + '</option>';
        });
        $('#shopCategory').html(tempHtml);
      } else {
        $.toast('获取失败：'+data.data);
      }
    });
  }

  $('#submit').click(function () {
    var shop = {};
    if (isEdit) {
      shop.shopId = shopId;
    }
    shop.shopName = $('#shopName').val();
    shop.shopAddr = $('#shopAddr').val();
    shop.shopPhone = $('#shopPhone').val();
    shop.shopDesc = $('#shopDesc').val();

    shop.shopCategory = {
      shopCategoryId: $('#shopCategory').find('option').not(function () {
        return !this.selected;
      }).data('id')
    };
    shop.area = {
      areaId: $('#area').find('option').not(function () {
        return !this.selected;
      }).data('id')
    };

    var shopImg = $('#shopImg')[0].files[0];

    var formData = new FormData();
    formData.append('file', shopImg);
    formData.append('shopJson', JSON.stringify(shop));
    var verifyCodeActual = $('#j_captcha').val();
    if (!verifyCodeActual) {
      $.toast('请输入验证码！');
      return;
    }
    formData.append("verifyCodeActual", verifyCodeActual);
    $.ajax({
      url: editShopUrl,
      type: 'POST',
      data: formData,
      contentType: false,
      processData: false,
      cache: false,
      success: function (data) {
        if (data.code == 0) {
          $.toast('提交成功！');
        } else {
          $.toast('提交失败：'+data.data);
        }
      }
    });
  });

})
package com.learn.service;

import com.learn.pojo.Area;
import com.learn.pojo.PersonInfo;
import com.learn.pojo.Shop;
import com.learn.pojo.ShopCategory;
import java.io.File;
import java.io.FileInputStream;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.learn.BaseTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;


public class ShopServiceTest extends BaseTest {

  @Autowired
  private ShopService shopService;

  @Test
  public void testAddShop() throws Exception {
    Shop shop = new Shop();
    PersonInfo personInfo = new PersonInfo();
    personInfo.setUserId(1L);

    Area area = new Area();
    area.setAreaId(1L);

    ShopCategory sc = new ShopCategory();
    sc.setShopCategoryId(1L);

    shop.setPersonInfo(personInfo);
    shop.setShopName("mytest1");
    shop.setShopDesc("mytest1");
    shop.setShopAddr("testaddr1");
    shop.setPhone("13810524526");
    shop.setPriority(10);
    shop.setLongitude(1D);
    shop.setLatitude(1D);
    shop.setCreateTime(new Date());
    shop.setLastEditTime(new Date());
    shop.setEnableStatus(0);
    shop.setAdvice("审核中");
    shop.setArea(area);
    shop.setShopCategory(sc);
    File file = new File("E://Downloads/1541405863-9907-img.jpg");
//    String s = shopService.addShop(shop,file);
//    assertEquals("mytest1", se.getShop().getShopName());
  }


  @Test
  public void testGetShopList() throws Exception {
    String result = shopService.getShopList(new Shop(), 2, 10);
    System.out.println(result);
  }

  @Test
  public void testGetByShopId() throws Exception {
    long shopId = 15;
    Shop shop = shopService.getByShopId(shopId);
    System.out.println(shop);
  }

}

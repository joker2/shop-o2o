package com.learn.dao;

import static org.junit.Assert.assertEquals;

import com.learn.BaseTest;
import com.learn.pojo.ProductCategory;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class ProductCategoryMapperTest extends BaseTest {

  @Autowired
  private ProductCategoryMapper productCategoryMapper;

  @Test
  public void testInsertProductCategory() throws Exception {
    ProductCategory productCategory = new ProductCategory();
    productCategory.setProductCategoryName("商品类别1");
    productCategory.setProductCategoryDesc("测试商品类别");
    productCategory.setPriority(1);
    productCategory.setCreateTime(new Date());
    productCategory.setLastEditTime(new Date());
    productCategory.setShopId(1L);
    ProductCategory productCategory2 = new ProductCategory();
    productCategory2.setProductCategoryName("商品类别2");
    productCategory2.setProductCategoryDesc("测试商品类别2");
    productCategory2.setPriority(2);
    productCategory2.setCreateTime(new Date());
    productCategory2.setLastEditTime(new Date());
    productCategory2.setShopId(1L);
    List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();
    productCategoryList.add(productCategory);
    productCategoryList.add(productCategory2);
    int effectedNum = productCategoryMapper.batchInsertProductCategory(productCategoryList);
    assertEquals(2, effectedNum);
  }

  @Test
  public void testQueryProductCategoryByShopId() throws Exception {
    long shopId = 1;
    List<ProductCategory> productCategoryList = productCategoryMapper
        .queryProductCategoryByShopId(shopId);
    assertEquals(2, productCategoryList.size());
    System.out.println(productCategoryList.get(0).toString());

  }

  @Test
  public void testDeleteProductCategory() throws Exception {
    long shopId = 1;
    List<ProductCategory> productCategoryList = productCategoryMapper
        .queryProductCategoryByShopId(shopId);
    int effectedNum = 0;
    for (ProductCategory productCategory : productCategoryList) {
      if ("商品类别1".equals(productCategory.getProductCategoryName()) || "商品类别2"
          .equals(productCategory.getProductCategoryName())) {
        effectedNum += productCategoryMapper
            .deleteProductCategory(productCategory.getProductCategoryId(), shopId);
      }
    }
    assertEquals(6, effectedNum);
  }
}

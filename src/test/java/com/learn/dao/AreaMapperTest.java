package com.learn.dao;

import static org.junit.Assert.assertEquals;

import com.learn.BaseTest;
import com.learn.pojo.Area;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AreaMapperTest extends BaseTest {
	@Autowired
	private AreaMapper areaMapper;

//	@Test
//	public void testAInsertArea() throws Exception {
//		Area area = new Area();
//		area.setAreaName("区域1");
//		area.setAreaDesc("区域1");
//		area.setPriority(1);
//		area.setCreateTime(new Date());
//		area.setLastEditTime(new Date());
//		int effectedNum = areaMapper.insertArea(area);
//		assertEquals(1, effectedNum);
//	}

	@Test
	public void testBQueryArea() throws Exception {
		List<Area> areaList = areaMapper.queryArea();
//		assertEquals(4, areaList.size());
	}

//	@Test
//	public void testCUpdateArea() throws Exception {
//		Area area = new Area();
//		area.setAreaId(1L);
//		area.setAreaName("南苑");
//		area.setLastEditTime(new Date());
//		int effectedNum = areaMapper.updateArea(area);
//		assertEquals(1, effectedNum);
//	}
//
//	@Test
//	public void testDDeleteArea() throws Exception {
//		long areaId = -1;
//		List<Area> areaList = areaMapper.queryArea();
//		for (Area myArea : areaList) {
//			if ("区域1".equals(myArea.getAreaName())) {
//				areaId = myArea.getAreaId();
//			}
//		}
//		List<Long> areaIdList = new ArrayList<Long>();
//		areaIdList.add(areaId);
//		int effectedNum = areaMapper.batchDeleteArea(areaIdList);
//		assertEquals(1, effectedNum);
//	}
}

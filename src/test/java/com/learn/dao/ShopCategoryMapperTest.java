package com.learn.dao;

import static org.junit.Assert.assertEquals;

import com.learn.BaseTest;
import com.learn.pojo.ShopCategory;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class ShopCategoryMapperTest extends BaseTest {

  @Autowired
  private ShopCategoryMapper shopCategoryMapper;

//  @Test
//  public void testAInsertShopCategory() throws Exception {
//    ShopCategory shopCategory = new ShopCategory();
//    shopCategory.setShopCategoryName("店铺类别1");
//    shopCategory.setShopCategoryDesc("测试商品类别");
//    shopCategory.setPriority(1);
//    shopCategory.setCreateTime(new Date());
//    shopCategory.setLastEditTime(new Date());
//    shopCategory.setParentId(1L);
//    int effectedNum = shopCategoryDao.insertShopCategory(shopCategory);
//    assertEquals(1, effectedNum);
//  }

  @Test
  public void testQueryShopCategory() throws Exception {
    ShopCategory shopCategory = new ShopCategory();
    List<ShopCategory> shopCategoryList = shopCategoryMapper.queryShopCategory(shopCategory);
    assertEquals(1, shopCategoryList.size());
//    shopCategory.setParentId(1L);
//    shopCategoryList = shopCategoryMapper.queryShopCategory(shopCategory);
//    assertEquals(1, shopCategoryList.size());
//    shopCategory.setParentId(null);
//    shopCategory.setShopCategoryId(0L);
//    shopCategoryList = shopCategoryMapper.queryShopCategory(shopCategory);
//    assertEquals(2, shopCategoryList.size());

  }

//  @Test
//  public void testCUpdateShopCategory() throws Exception {
//    ShopCategory shopCategory = new ShopCategory();
//    shopCategory.setShopCategoryId(1L);
//    shopCategory.setShopCategoryName("把妹");
//    shopCategory.setShopCategoryDesc("把妹");
//    shopCategory.setLastEditTime(new Date());
//    int effectedNum = shopCategoryDao.updateShopCategory(shopCategory);
//    assertEquals(1, effectedNum);
//  }
//
//  @Test
//  public void testDDeleteShopCategory() throws Exception {
//    ShopCategory sc = new ShopCategory();
//    sc.setParentId(1L);
//    List<ShopCategory> shopCategoryList = shopCategoryDao
//        .queryShopCategory(sc);
//    long shopCategoryId = shopCategoryList.get(0).getShopCategoryId();
//    int effectedNum = shopCategoryDao.deleteShopCategory(shopCategoryId);
//    assertEquals(1, effectedNum);
//  }

}

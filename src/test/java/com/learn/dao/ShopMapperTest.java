package com.learn.dao;

import static org.junit.Assert.assertEquals;

import com.learn.BaseTest;
import com.learn.pojo.Area;
import com.learn.pojo.PersonInfo;
import com.learn.pojo.Shop;
import com.learn.pojo.ShopCategory;
import java.util.Date;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class ShopMapperTest extends BaseTest {

  @Autowired
  private ShopMapper shopMapper;

  @Test
  public void testAInsertShop() throws Exception {
    Shop shop = new Shop();
    PersonInfo personInfo = new PersonInfo();
    personInfo.setUserId(1L);

    Area area = new Area();
    area.setAreaId(4L);

    ShopCategory sc = new ShopCategory();
    sc.setShopCategoryId(1L);

    shop.setPersonInfo(personInfo);
    shop.setShopName("mytest1");
    shop.setShopDesc("mytest1");
    shop.setShopAddr("testaddr1");
    shop.setPhone("13810524526");
//    shop.setShopImg("test1");
    shop.setPriority(10);
    shop.setLongitude(1D);
    shop.setLatitude(1D);
    shop.setCreateTime(new Date());
    shop.setLastEditTime(new Date());
    shop.setEnableStatus(0);
    shop.setAdvice("审核中");
    shop.setArea(area);
    shop.setShopCategory(sc);
    int effectedNum = shopMapper.insertShop(shop);
    assertEquals(1, effectedNum);
  }

//	@Test
//	public void testBQueryByEmployeeId() throws Exception {
//		long employeeId = 1;
//		List<Shop> shopList = shopDao.queryByEmployeeId(employeeId);
//		for (Shop shop : shopList) {
//			System.out.println(shop);
//		}
//	}
//
//	@Test
//	public void testBQueryShopList() throws Exception {
//		Shop shop = new Shop();
//		List<Shop> shopList = shopDao.queryShopList(shop, 0, 2);
//		assertEquals(2, shopList.size());
//		int count = shopDao.queryShopCount(shop);
//		assertEquals(3, count);
//		shop.setShopName("花");
//		shopList = shopDao.queryShopList(shop, 0, 3);
//		assertEquals(2, shopList.size());
//		count = shopDao.queryShopCount(shop);
//		assertEquals(2, count);
//		shop.setShopId(1L);
//		shopList = shopDao.queryShopList(shop, 0, 3);
//		assertEquals(1, shopList.size());
//		count = shopDao.queryShopCount(shop);
//		assertEquals(1, count);
//
//	}
//
	@Test
	public void testCQueryByShopId() throws Exception {
		long shopId = 15;
		Shop shop = shopMapper.queryByShopId(shopId);
		System.out.println(shop);
	}

	@Test
	public void testUpdateShop() {
    Shop shop = new Shop();
		shop.setShopId(30L);
		shop.setShopName("四季花");
		int effectedNum = shopMapper.updateShop(shop);
		assertEquals(1, effectedNum);
	}
//
//	@Test
//	public void testEDeleteShopByName() throws Exception {
//		String shopName = "mytest1";
//		int effectedNum = shopDao.deleteShopByName(shopName);
//		assertEquals(1, effectedNum);
//
//	}

}
